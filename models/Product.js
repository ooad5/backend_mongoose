const mongoose = require('mongoose')
const { Schema } = mongoose
const productShema = Schema({
  name: String,
  price: Number
})
module.exports = mongoose.model('Product', productShema)
