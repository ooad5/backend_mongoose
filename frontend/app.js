const express = require('express')
const app = express()
const port = 3000
// Middleware
app.get('/', function (req, res) {
  console.log(req)
  res.json({
    id: 2000,
    name: 'Iphone'
  })
})

app.get('/hellome', function (req, res) {
  res.send('Hello Me')
})

app.listen(port, function () {
  // eslint-disable-next-line no-template-curly-in-string
  console.log('Example app listening on port ${port}')
})
